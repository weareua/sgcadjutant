import random
from telegram import InputMediaPhoto
from Components.Lingua import greetings_list, thx_list, codex_list
from Services.MeaningSearch import get_meaning
from Services.TimeManagement import typing_time
from Services.ImageSearch import get_image


def start(bot, update):
    chat_id = update.message.chat_id
    start_text = "Мої вітання, {0}. ".format(
        update.message.from_user.first_name) + "\n" + \
        "Головно, я є частиною Малої Галактичної Ради." + \
        " Можете спробувати долучитися за посиланням:" + \
        " https://t.me/joinchat/EO9Rg0GzT8IAm3Uh-gltSA" + "\n" + \
    typing_time(bot, chat_id)
    bot.sendMessage(
        chat_id=update.message.chat_id,
        text=start_text)


def hi(bot, update):
    chat_id = update.message.chat_id

    attr_list = [
        random.choice(greetings_list), update.message.from_user.first_name]
    random.shuffle(attr_list)
    greetings_text = "{0}, {1}!".format(attr_list[0], attr_list[1])
    if not greetings_text[0].isupper():
        greetings_text = greetings_text[:1].capitalize() + greetings_text[1:]

    typing_time(bot, chat_id)
    bot.sendMessage(chat_id=chat_id, text=greetings_text)


def thx(bot, update):
    chat_id = update.message.chat_id
    attr_list = [
        random.choice(thx_list), update.message.from_user.first_name]
    random.shuffle(attr_list)
    thx_text = "{0}, {1}.".format(attr_list[0], attr_list[1])
    if not thx_text[0].isupper():
        thx_text = thx_text[:1].capitalize() + thx_text[1:]

    typing_time(bot, chat_id)
    bot.sendMessage(
        chat_id=update.message.chat_id,
        text=thx_text)

def optional_talks(bot, update):
    """
    Make sure that bot talks not every time
    """
    quess_list = range(2)
    quess = random.choice(quess_list)
    if quess in [1,2]:
        thx(bot, update)


def meaning(bot, update, actual_query, lang):
    chat_id = update.message.chat_id
    err_text = 'На жаль, мені ' + \
        'не вдалося знайти жодного запису з цього приводу.'
    try:
        typing_time(bot, chat_id)
        text = get_meaning(actual_query, lang)
        if not text:
            text = err_text
        bot.sendMessage(
            chat_id=chat_id,
            text=text)
    except Exception:
        # TODO: handle it in a proper way
        typing_time(bot, chat_id)
        bot.sendMessage(
            chat_id=chat_id,
            text=err_text)

def whole_codex(bot, update):
    chat_id = update.message.chat_id
    text_response = (("\n " * 2).join(codex_list))
    typing_time(bot, chat_id)
    bot.sendMessage(chat_id=chat_id, text=text_response)


def rule_codex(bot, update, actual_rule_index):
    chat_id = update.message.chat_id
    actual_rule_index = int(actual_rule_index)
    if actual_rule_index < 1 or actual_rule_index > 20:
        text_response = '{0}, Кодекс Малої Галактичної Ради налічує лише 20 пунктів.'.format(update.message.from_user.first_name) # noqa
    else:
        text_response = codex_list[actual_rule_index]
    typing_time(bot, chat_id)
    bot.sendMessage(chat_id=chat_id, text=text_response)


def pic(bot, update, actual_query):
    chat_id = update.message.chat_id
    text = 'На жаль, керівництво Microsoft вирішило відмовити у безкоштовному ' + \
        'доступі до Bing Image Search API, який я використовував для пошуку фото.'
    
    typing_time(bot, chat_id)
    bot.sendMessage(
        chat_id=chat_id,
        text=text)
